'use strict';

var assign = require('object-assign');
var docsOpts = require('../../lib/docs-opts');
var del = require('del');
var metalsmith = require('metalsmith');
var metalsmithLayouts = require('metalsmith-layouts');
var metalsmithMarkdown = require('metalsmith-markdown');
var metalsmithTextReplace = require('metalsmith-text-replace');
var opts = require('gulp-auto-task').opts();
var pkg = require('../../../package.json');

module.exports = function docsMetalsmith (done) {
    opts = assign(docsOpts, opts);

    var localDistLocation = '//' + opts.host + ':' + opts.port + '/dist/aui';
    var ms = metalsmith('.tmp/docs')
        .destination('./dist')
        .use(metalsmithMarkdown())
        .use(metalsmithLayouts({
            directory: 'layouts',
            engine: 'handlebars'
        }))
        .use(metalsmithTextReplace({
            '**/**/*.html': [{
                find: /\$\{project\.version\}/g,
                replace: pkg.version
            }, {
                find: /\$\{dist\.location\}/g,
                replace: opts.localdist ? localDistLocation : ('//aui-cdn.atlassian.com/aui-adg/' + pkg.version)
            }]
        }));

    ms.build(function (err) {
        if (err) {
            throw err;
        }
        del([
            '.tmp/docs/dist/scripts/*',
            '.tmp/docs/dist/styles/*.less'
        ]).then(() => done());
    });
};
