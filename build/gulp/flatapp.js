'use strict';

var gat = require('gulp-auto-task');
var gulp = require('gulp');

module.exports = gulp.series(
    gat.load('i18n'),
    gat.load('dist'),
    gat.load('flatapp/copy'),
    gat.load('flatapp/soy-assets'),
    gat.load('flatapp/build')
);
