'use strict';

var gat = require('gulp-auto-task');
var path = require('path');
var pkg = require('../../../package.json');
var soyCli = require('atlassian-soy-cli');
var soyVersion = require(path.join('@atlassian', 'soy-template-plugin-js', 'package.json')).version;
var opts = gat.opts();

module.exports = function flatappBuild (done) {
    var soyOpts = {
        baseDir: path.join('.tmp', 'flatapp', 'src', 'soy', 'pages'),
        i18nBundle: '.tmp/i18n.properties',
        glob: '**.soy',
        outDir: path.join('.tmp', 'flatapp', 'target', 'static', 'pages'),
        rootNamespace: 'testPages.pages',
        data: {
            auiVersion: pkg.version,
            language: 'en'
        },
        dependencies: [{
            // AUI soy templates
            baseDir: path.join(opts.root, 'src/soy'),
            glob: '**.soy'
        }, {
            // flatapp dependencies
            baseDir: path.join('.tmp', 'flatapp', 'src', 'soy', 'dependencies'),
            glob: '**.soy'
        }]
    };

    var params = {
        version: soyVersion,
        basedir: soyOpts.baseDir,
        outdir: soyOpts.outDir,
        type: 'render',
        extraArgs: {
            i18n: soyOpts.i18nBundle
        }
    };

    if (soyOpts.outputExtension) {
        params.extraArgs.extension = soyOpts.outputExtension;
    }

    if (soyOpts.rootNamespace) {
        params.extraArgs.rootnamespace = soyOpts.rootNamespace;
    }

    if (soyOpts.dependencies) {
        var depString = soyOpts.dependencies.map(function(dep) {
            return dep.baseDir + ':' + dep.glob;
        }).join(',');
        params.extraArgs.dependencies = depString;
    }

    if (soyOpts.data) {
        var dataString = Object.keys(soyOpts.data).map(function(key) {
            return key + ':' + soyOpts.data[key];
        }).join(',');
        params.extraArgs.data = dataString;
    }

    soyCli(params).compile(soyOpts.glob).then(function() {
        done();
    }, done);
};
