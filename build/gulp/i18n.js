'use strict';

var gat = require('gulp-auto-task');
var galv = require('galvatron');
var gulp = require('gulp');
var gulpBabel = require('gulp-babel');
var gulpDebug = require('gulp-debug');
var gulpRename = require('gulp-rename');
var path = require('path');
var through = require('through2');

var opts = gat.opts();

module.exports = function i18n () {
    return galv.trace(path.join(opts.root, 'src/js/aui/internal/i18n/aui.js')).createStream()
        .pipe(gulpDebug({title: 'i18n'}))
        .pipe(galv.cache('babel-umd', gulpBabel({
            presets: ['es2015'],
            plugins: [
                'add-module-exports',
                'transform-es2015-modules-umd'
            ]
        })))
        .pipe(gulpRename({dirname: '', basename: 'i18n', extname: '.properties'}))

        // save so we can require it in the next pipe
        .pipe(gulp.dest('.tmp'))

        // required in here so that it's parsed as javascript
        .pipe(through.obj(function (file, type, done) {
            var keys = require(file.path);
            var props = '';

            for (var key in keys) {
                if (Object.prototype.hasOwnProperty.call(keys, key)) {
                    props += key + ' = ' + keys[key] + '\n';
                }
            }

            file.contents = new Buffer(props);
            this.push(file);
            done();
        }))

        // overwrite the transpiled file
        .pipe(gulp.dest('.tmp'))
};
