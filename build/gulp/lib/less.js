var assign = require('object-assign');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpDebug = require('gulp-debug');
var gulpLess = require('gulp-less');
var gulpSourcemaps = require('gulp-sourcemaps');
var path = require('path');

var opts = gat.opts();

module.exports = gulp.parallel(
    function fonts () {
        return gulp.src([
            'src/less/fonts/**',
            path.join(opts.root, 'src/less/fonts/**')
        ])
            .pipe(gulpDebug({title: 'lib/fonts'}))
            .pipe(gulp.dest('lib/css/fonts'));
    },
    function images () {
        return gulp.src([
            'src/less/images/**',
            path.join(opts.root, 'src/less/images/**')
        ])
            .pipe(gulpDebug({title: 'lib/images'}))
            .pipe(gulp.dest('lib/css/images'));
    },
    function less () {
        return gulp.src([
            'src/less/**/*.less',
            path.join(opts.root, 'src/less/**/*.less')
        ])
            .pipe(gulpDebug({title: 'lib/less'}))
            .pipe(gulpSourcemaps.init())
            .pipe(gulpLess())
            .pipe(gulpSourcemaps.write('.'))
            .pipe(gulp.dest('lib/css'));
    },
    function select2Images () {
        return gulp.src([
            'src/css-vendor/jquery/plugins/*.png',
            'src/css-vendor/jquery/plugins/*.gif',
            path.join(opts.root, 'src/css-vendor/jquery/plugins/*.png'),
            path.join(opts.root, 'src/css-vendor/jquery/plugins/*.gif')
        ])
            .pipe(gulpDebug({title: 'lib/select-2-images'}))
            .pipe(gulp.dest('lib/css'));
    }
);
