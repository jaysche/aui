'use strict';

var gat = require('gulp-auto-task');
var gulp = require('gulp');

module.exports = gulp.parallel(
    gat.load('lint/jscs'),
    gat.load('lint/eslint')
);
