'use strict';

var assign = require('object-assign');
var KarmaServer = require('karma').Server;

module.exports = function (opts, done) {
    opts = assign({
        browsers: 'Chrome_1024x768,Firefox_1024x768',
        logs: false,
        reporters: 'progress,junit',
        watch: false
    }, opts);

    var config = {
        hostname: opts.watch ? '0.0.0.0' : 'localhost',
        autoWatch: !!opts.watch,
        singleRun: !opts.watch,
        frameworks: ['mocha', 'sinon-chai'],
        browsers: opts.browsers.split(','),
        customLaunchers: {
            // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
            Chrome_1024x768: {
                base: 'Chrome',
                flags: ['--window-size=1024,768']
            },
            Firefox_1024x768: {
                base: 'Firefox',
                flags: ['-foreground', '-width', '1024', '-height', '768']
            }
        },
        reporters: opts.reporters.split(','),
        client: {
            args: opts.grep ? ['--grep', opts.grep] : [],
            captureConsole: !!opts.logs
        },
        coverageReporter: {
            dir: 'reports/istanbul',
            type: 'html'
        },
        junitReporter: {
            outputDir: 'tests',
            suite: ''
        },
        preprocessors: {
            'src/less/{**/*,*}.less': 'less'
        },
        lessPreprocessor: {
            options: {
                paths: ['src/less/**'],
                save: false,
                relativeUrls: true
            }
        },
        files: [
            'src/less/batch/aui-experimental.less',
            'src/less/batch/aui.less',
            'tests/styles/all.css',

            // Finds the file when running tests from the aui-adg repo.
            'node_modules/@atlassian/aui/tests/styles/all.css',

            // Depending on which jQuery version has been installed by Bower the
            // dist file might be in a different location. Since we should only
            // have one version of jQuery installed at a given time, only one
            // of these paths should resolve.
            'bower_components/jquery/jquery.js',
            'bower_components/jquery/dist/jquery.js',

            'bower_components/jquery-migrate/jquery-migrate.js',
            '.tmp/unit.js',
            {
                pattern: 'src/less/images/**/*.*',
                watched: false,
                included: false,
                served: true,
                nocache: false
            },
            {
                pattern: 'src/css-vendor/jquery/plugins/*.*',
                watched: false,
                included: false,
                served: true,
                nocache: false
            }
        ]
    };

    if (opts.saucelabs) {
        var saucelabsLaunchers = require('../lib/saucelabs-launchers');
        config = assign(config, {
            sauceLabs: {
                testName: 'AUI unit tests (5.9.x)',
                recordScreenshots: false,
                connectOptions: {
                    verbose: true,
                    verboseDebugging: true
                }
            },
            customLaunchers: saucelabsLaunchers,
            browsers: Object.keys(saucelabsLaunchers),
            captureTimeout: 120000,
            reporters: ['saucelabs', 'dots', 'junit'],
            autoWatch: false,
            singleRun: true,
            concurrency: 5,
            client: {
                captureConsole: false
            }
        });
        delete config.hostname;
    }

    return new KarmaServer(config, function (code) {
        done();
        process.exit(code);
    });
};
