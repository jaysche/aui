---
component: Buttons
layout: main-layout.html
analytics:
  pageCategory: component
  component: button
---
<a href="https://design.atlassian.com/latest/product/components/buttons/" class="aui-button aui-button-link docs-meta-link">Design guidelines</a>

<h3>Summary</h3>

<p>Use buttons as triggers for actions that are used in forms, toolbars, dialog footers and as stand-alone action triggers. Try to avoid the usage of buttons for navigation. The main difference between actions and navigation is that <strong>Actions</strong> are operations performed on objects, while <strong>Navigation</strong> refers to elements on the screen or view that take you to another context in the application.

<h3>Status</h3>
<table class="aui summary">
    <tbody>
        <tr>
            <th>API status:</th>
            <td><span class="aui-lozenge aui-lozenge-success">general</span></td>
        </tr>
        <tr>
            <th>Included in AUI core?</th>
            <td>Yes. You do not need to explicitly require the web resource key.</td>
        </tr>
        <tr>
            <th>Web resource key:</th>
            <td class="resource-key" data-resource-key="com.atlassian.auiplugin:aui-buttons"><code>com.atlassian.auiplugin:aui-buttons</code></td>
        </tr>
        <tr>
            <th>Experimental since:</th>
            <td>4.2</td>
        </tr>
        <tr>
            <th>General API status:</th>
            <td>5.1</td>
        </tr>
    </tbody>
</table>

<h3>Button types</h3>

<div class="aui-flatpack-example">
    <p>
        <button class="aui-button">Standard button</button>
    </p>

    <p>
        <button class="aui-button aui-button-primary">Primary Button </button>
    </p>

    <p>
        <button class="aui-button aui-button-link">Link button</button>
    </p>

    <p>
        <button class="aui-button aui-dropdown2-trigger" aria-owns="dropdown2-more" aria-haspopup="true">Dropdown button</button>
            </p><div id="dropdown2-more" class="aui-dropdown2 aui-style-default">
                <ul class="aui-list-truncate">
                    <li><a href="http://example.com/">Menu item 1</a></li>
                    <li><a href="http://example.com/">Menu item 2</a></li>
                    <li><a href="http://example.com/">Menu item 3</a></li>
                </ul>
            </div>
            </div>
        <p></p>
<h3>Button variations</h3>

<p>These variations can be used against all button types. For button groups on your page, only choose one type of variation, do not mix them.</p>

<div class="aui-flatpack-example"> 
    <p>
        <button class="aui-button">Label only</button>
    </p>

    <p>
        <button class="aui-button aui-button-light">Light button</button>
    </p>

    <p>
        <button class="aui-button"><span class="aui-icon aui-icon-small aui-iconfont-configure"></span> Icon and label</button>
    </p>

    <p>
        <button class="aui-button"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
    </p>

    <p>
        <button class="aui-button aui-button-subtle"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span>Subtle button</button>
    </p>

    <p>
        <button class="aui-button" aria-disabled="true">Disabled button</button>
    </p>
    <p>
        <div class="aui-buttons">
        <button class="aui-button">Tool</button>
        <button class="aui-button">Bar</button>
        </div>
    </p>
    <p>
        <div id="split-container" class="aui-buttons">
            <button class="aui-button aui-button-split-main">Split button</button>
            <button class="aui-button aui-dropdown2-trigger aui-button-split-more"
                    aria-controls="dropdown-button-split">Split More</button>
        </div>
        <div id="dropdown-button-split" class="aui-dropdown2 aui-style-default"
             data-aui-alignment-container="#split-container">
            <ul>
                <li><a href="#">Menu item 1</a></li>
                <li><a href="#">Menu item 2</a></li>
                <li><a href="#">Menu item 3</a></li>
            </ul>
        </div>
    </p>

    <p>
        <button class="aui-button aui-button-compact">Compact</button>
        <button aria-controls="compact-button-dropdown"
                class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger-arrowless">
            <span class="aui-icon aui-icon-small aui-iconfont-more">More</span>
        </button>
        <div id="compact-button-dropdown" class="aui-dropdown2 aui-style-default">
            <ul>
                <li><a href="#" class="active">Menu item 1</a></li>
                <li><a href="#" class="">Menu item 2</a></li>
                <li><a href="#" class="">Menu item 3</a></li>
            </ul>
        </div>
    </p>
</div>

<h3>Code examples</h3>

<h4>HTML</h4>

<p>The base button code is:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button">Button</button>
    </noscript>
</aui-docs-example>

<p>You can then apply a button type by adding the appropriate class, for example <code>aui-button-primary</code>:</p>

<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button aui-button-primary">Button</button>
    </noscript>
</aui-docs-example>

<h4>Button types and classes</h4>

<ul>
    <li>Standard/default - (no extra class)</li>
    <li>Primary - <code>aui-button-primary</code></li>
    <li>Link-style (used for "cancel" actions) - <code>aui-button-cancel</code></li>
    <li>Subtle (looks like a link while inactive, looks like a button when hovered/focused) - <code>aui-button-subtle</code></li>
    <li>Split (has a primary and secondary area within one button)</li>
</ul>

<h4>Button states</h4>
<p>Button states are applied using boolean ARIA attributes:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <button class="aui-button" aria-disabled="true" disabled>Button</button>
        <button class="aui-button" aria-pressed="true">Button</button>
    </noscript>
</aui-docs-example>

<p>States:</p>

<ul>
    <li>Disabled: Buttons provides the disabled <em>style</em> but you still need to disable the events - <code>aria-disabled="true"</code>.</li>
    <li>Pressed: A pressed/enabled style for toggle buttons - <code>aria-pressed="true"</code></li>
</ul>

<p>Note: The style applies when the attribute is present and set to true.</p>
<p><strong>Button groups</strong></p>
<p>Create a button group by wrapping buttons in an <code>aui-buttons</code> (note plural) DIV element:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-buttons">
            <button class="aui-button">Button</button>
            <button class="aui-button">Button</button>
            <button class="aui-button">Button</button>
        </div>
    </noscript>
</aui-docs-example>

<p><strong>Split buttons</strong></p> 
<p>Require a wrapper and extra modifier classes; the second button should always be a Dropdown2 trigger:</p>
<aui-docs-example>
    <noscript is="aui-docs-code" type="text/html">
        <div class="aui-buttons">
            <button class="aui-button aui-button-split-main">Split main</button>
            <button class="aui-button aui-dropdown2-trigger aui-button-split-more" aria-haspopup="true" aria-owns="split-container-dropdown">Split more</button>
            <div id="split-container-dropdown" class="aui-dropdown2 aui-style-default" data-container="split-button-demo"
            role="menu" aria-hidden="true">
                <ul class="aui-list-truncate">
                    <li><a href="http://example.com/">Menu item 1</a></li>
                    <li><a href="http://example.com/">Menu item 2</a></li>
                    <li><a href="http://example.com/">Menu item 3</a></li>
                </ul>
            </div>
        </div>
    </noscript>
</aui-docs-example>

<p>Note: Use the Dropdown2's <code>data-container</code> feature to force the dropdown to extend right to left by setting the container with an ID on the <code>aui-buttons</code> element.</p>

<h4>Soy</h4>

<h5>Single button</h5>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Button'/}
    {/call}

    {call aui.buttons.button}
        {param text: 'Primary Button'/}
        {param type: 'primary'/}
    {/call}
</noscript>

<h5>Dropdown 2 button</h5>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Dropdown button'/}
        {param type: 'link'/}
        {param dropdown2Target: 'dropdown2id'/}
    {/call}
</noscript>
<h5>Icon button</h5>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: ' Icon Button' /}
        {param iconType: 'aui' /}
        {param iconClass: 'aui-icon-small aui-iconfont-view' /}
        {param iconText: 'View' /}
    {/call}
</noscript>
<h5>Grouped buttons</h5</p>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.buttons}
        {param content}
            {call aui.buttons.button}{param text: 'Button'/}{/call}
            {call aui.buttons.button}{param text: 'Button'/}{/call}
            {call aui.buttons.button}{param text: 'Button'/}{/call}
        {/param}
    {/call}
</noscript>

<h5>Split buttons</h5>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.buttons}
        {param content}
            {call aui.buttons.splitButton}
                {param splitButtonMain: [
                    'text': 'Split main'
                ] /}
                {param splitButtonMore: [
                    'text': 'Split more',
                    'dropdown2Target': 'split-container-dropdown'
                ] /}
            {/call}
        {/param}
    {/call}
</noscript>

<h5>Disabled buttons</h5>

<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Button'/}
        {param isDisabled: 'true'/}
    {/call}
</noscript>

<h5>Pressed buttons</h5>
<noscript is="aui-docs-code" type="text/handlebars">
    {call aui.buttons.button}
        {param text: 'Button'/}
        {param isPressed: 'true'/}
    {/call}
</noscript>