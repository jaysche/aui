'use strict';

import $ from '../jquery';
var isMacOSX = (/Mac OS X/.test(navigator.userAgent));

export function supportsCalc () {
    var $d = $('<div style="height: 10px; height: -webkit-calc(20px + 0); height: calc(20px);"></div>');
    var supportsCalc = (20 === $d.appendTo(document.documentElement).height());
    $d.remove();

    return supportsCalc;
}

export function supportsRequestAnimationFrame () {
    return !!window.requestAnimationFrame;
}

export function supportsVoiceOver () {
    return isMacOSX;
}


// This is supported everywhere except Chrome 22, but we needed to support this use case due to
// https://bitbucket.org/atlassian/aui/pull-requests/1920/aui-4380-fix-shortcut-not-work-in-old/diff .
export function supportsNewMouseEvent () {
    try {
        new MouseEvent('click');
    } catch(e) {
        return false;
    }

    return true;
}
