'use strict';

import * as blanket from '../../../src/js/aui/blanket';
import $ from '../../../src/js/aui/jquery';

function getBlanketZIndex () {
    return blanket.dim.$dim.css('z-index');
}

describe('aui/blanket', function () {
    var clock;

    beforeEach(function () {
        clock = sinon.useFakeTimers();
        document.body.style = '';
    });

    afterEach(function () {
        clock.restore();
    });

    it('globals', function () {
        expect(AJS.dim).to.equal(blanket.dim);
        expect(AJS.undim).to.equal(blanket.undim);
    });

    it('dim() will update the z-index accordingly', function () {
        blanket.dim(false, 5);
        expect(getBlanketZIndex()).to.equal('5');
        blanket.dim(false, 10);
        expect(getBlanketZIndex()).to.equal('10');
    });

    it('dim() will not created multiple blankets on duplicate calls', function () {
        blanket.dim();
        blanket.dim();
        expect($('.aui-blanket').length).to.equal(1);
    });

    it('dim() and undim() will hide all blankets that were previously visible', function () {
        blanket.dim();
        blanket.undim();

        var noBlanket = !blanket.dim.$dim;
        var invisibleBlanket = blanket.dim.$dim.attr('aria-hidden') === 'true';
        expect(noBlanket || invisibleBlanket).to.be.true;
    });

    it('dim() will place aria-hidden="true" on an element that is a direct descendant of body', function () {
        var page = document.createElement('div');
        document.body.appendChild(page);
        blanket.dim();
        expect(page.getAttribute('aria-hidden')).to.equal('true');
        document.body.removeChild(page);
    });

    it('dim() will not place aria-hidden="true" on an element that is a direct descendant of body and has an aria-hidden attribute', function () {
        var divWithAriaHiddenFalse = document.createElement('div');
        var divWithAriaHiddenTrue = document.createElement('div');
        divWithAriaHiddenFalse.setAttribute('aria-hidden', 'false');
        divWithAriaHiddenTrue.setAttribute('aria-hidden', 'true');
        document.body.appendChild(divWithAriaHiddenFalse);
        document.body.appendChild(divWithAriaHiddenTrue);
        blanket.dim();
        expect(divWithAriaHiddenFalse.getAttribute('aria-hidden')).to.equal('false');
        expect(divWithAriaHiddenTrue.getAttribute('aria-hidden')).to.equal('true');
        document.body.removeChild(divWithAriaHiddenFalse);
        document.body.removeChild(divWithAriaHiddenTrue);
    });

    it('dim() will not place aria-hidden="true" on an element that is a direct descendant of body and is an aui-layer', function () {
        var divWithAuiLayer = document.createElement('div');
        divWithAuiLayer.classList.add('aui-layer')
        document.body.appendChild(divWithAuiLayer);
        blanket.dim();
        expect(divWithAuiLayer.getAttribute('aria-hidden')).to.equal(null);
        document.body.removeChild(divWithAuiLayer);

    });

    it('unDim() will place remove aria-hidden from an element that is a direct descendant of body', function () {
        var page = document.createElement('div');
        document.body.appendChild(page);
        blanket.dim();
        blanket.undim();
        expect(page.getAttribute('aria-hidden')).to.equal(null);
        document.body.removeChild(page);
    });

    it('unDim() does not remove aria-hidden from an element that already had aria-hidden when dim was called', function () {
        var divWithAriaHiddenFalse = document.createElement('div');
        var divWithAriaHiddenTrue = document.createElement('div');
        divWithAriaHiddenFalse.setAttribute('aria-hidden', 'false');
        divWithAriaHiddenTrue.setAttribute('aria-hidden', 'true');
        document.body.appendChild(divWithAriaHiddenFalse);
        document.body.appendChild(divWithAriaHiddenTrue);
        blanket.dim();
        blanket.undim();
        expect(divWithAriaHiddenFalse.getAttribute('aria-hidden')).to.equal('false');
        expect(divWithAriaHiddenTrue.getAttribute('aria-hidden')).to.equal('true');
    });

    it('should ensure that the blanket is not displayed after hidden', function () {
        blanket.dim();
        blanket.undim();

        clock.tick(1000);
        expect(blanket.dim.$dim.css('visibility')).to.equal('hidden');
    });

    it('Calling dim() twice restores correctly', function () {
        var divAddedBeforeDim = document.createElement('div');
        var divAddedAfterDim = document.createElement('div');
        document.body.appendChild(divAddedBeforeDim);
        blanket.dim();
        document.body.appendChild(divAddedAfterDim);
        blanket.dim();
        blanket.undim();
        expect(divAddedBeforeDim.getAttribute('aria-hidden')).to.equal(null);
        expect(divAddedAfterDim.getAttribute('aria-hidden')).to.equal(null);
        expect($('body').css('overflow')).to.eql('visible');
    });

    describe('overflow', function () {
        describe('on <body>', function () {
            it('is reverted to visible on undim if it was originally visible', function () {
                $('body').css({overflow: ''});
                blanket.dim();
                blanket.undim();
                expect($('body').css('overflow')).to.equal('visible');
            });

            it('is set to hidden on undim if it was originally hidden', function () {
                $('body').css({overflow: 'hidden'});
                blanket.dim();
                blanket.undim();
                expect($('body').css('overflow')).to.equal('hidden');
            });

            it('does not override overflow-x on undim', function () {
                $('body').css({'overflow-x': 'hidden'});

                const originalOverflow = document.body.style.overflow;
                const computedOverflow = window.getComputedStyle(document.body).overflow;

                blanket.dim();
                blanket.undim();

                expect($('body').css('overflow-x')).to.equal('hidden');
                expect(document.body.style.overflow).to.equal(originalOverflow);
                expect(window.getComputedStyle(document.body).overflow).to.equal(computedOverflow);
            });

            it('does not override overflow-y on undim', function () {
                $('body').css({'overflow-y': 'hidden'});

                const originalOverflow = document.body.style.overflow;
                const computedOverflow = window.getComputedStyle(document.body).overflow;

                blanket.dim();
                blanket.undim();

                expect($('body').css('overflow-y')).to.equal('hidden');
                expect(document.body.style.overflow).to.equal(originalOverflow);
                expect(window.getComputedStyle(document.body).overflow).to.equal(computedOverflow);
            });
        });

        describe('on <html>', function () {
            it('is reverted to visible on undim if it was originally visible', function () {
                //have to query the dom for the default in this case because ie7 does not adhere to the spec
                var overrideDefault = $('html').css('overflow');
                blanket.dim();
                blanket.undim();
                expect($('html').css('overflow')).to.equal(overrideDefault);
            });

            it('is set to hidden on undim if it was originally hidden', function () {
                $('html').css({overflow: 'hidden'});
                blanket.dim();
                blanket.undim();
                expect($('html').css('overflow')).to.equal('hidden');
            });

            it('does not override overflow-x on undim', function () {
                $('html').css({'overflow-x': 'hidden'});

                const originalOverflow = document.documentElement.style.overflow;
                const computedOverflow = window.getComputedStyle(document.documentElement).overflow;

                blanket.dim();
                blanket.undim();

                expect($('html').css('overflow-x')).to.equal('hidden');
                expect(document.documentElement.style.overflow).to.equal(originalOverflow);
                expect(window.getComputedStyle(document.documentElement).overflow).to.equal(computedOverflow);
            });

            it('does not override overflow-y on undim', function () {
                $('html').css({'overflow-y': 'hidden'});

                const originalOverflow = document.documentElement.style.overflow;
                const computedOverflow = window.getComputedStyle(document.documentElement).overflow;

                blanket.dim();
                blanket.undim();

                expect($('html').css('overflow-y')).to.equal('hidden');
                expect(document.documentElement.style.overflow).to.equal(originalOverflow);
                expect(window.getComputedStyle(document.documentElement).overflow).to.equal(computedOverflow);
            });
        });
    });
});
