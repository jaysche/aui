'use strict';

import debounce, { debounceImmediate } from '../../../src/js/aui/debounce';

describe('aui/debounce', function () {
    it('globals', function () {
        expect(AJS.debounce).to.equal(debounce);
        expect(AJS.debounceImmediate).to.equal(debounceImmediate);
    });
});
